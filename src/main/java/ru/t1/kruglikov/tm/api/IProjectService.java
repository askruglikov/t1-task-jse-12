package ru.t1.kruglikov.tm.api;

import ru.t1.kruglikov.tm.enumerated.Status;
import ru.t1.kruglikov.tm.model.Project;

public interface IProjectService extends IProjectRepository {

    Project create(String name, String description);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    Project changeStatusByIndex(Integer index, Status status);

    Project changeStatusById(String id, Status status);

}
